# A small guideline for new application
1. create applicaiton
1. meteor add twbs:bootstrap aldeed:collection2 aldeed:autoform aldeed:delete-button momentjs:moment accounts-password ian:accounts-ui-bootstrap-3 natestrauser:font-awesome iron:router
1. meteor remove autopublish insecure
1. add a collection 
1. define schema
1. create controller
1. add templates for the controller
1. add routes
1. secure route
1. implement the controller


## This skeleton with one collection "issues" & conforms the above guidelines.  

Uses following packages:
***
twbs:bootstrap 
***
iron:router
***
aldeed:collection2 
***
aldeed:autoform 
***
aldeed:delete-button 
***
momentjs:moment 
***
accounts-password 
***
ian:accounts-ui-bootstrap-3 
***
natestrauser:font-awesome
***

It doesn't include 
- autopublish
- insecure