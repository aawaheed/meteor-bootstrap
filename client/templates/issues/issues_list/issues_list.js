/*****************************************************************************/
/* IssueList: Event Handlers */
/*****************************************************************************/

/*****************************************************************************/
/* IssueList: Helpers */
/*****************************************************************************/


Template.IssuesList.helpers({
  issues: function () {
    return Issues.find();
  },

  dueDateFormatted: function () {
    return moment(this.dueDate).format("MMM Do YY");
  },

  priorityHigh: function() {
    if (this.priority === 'High')
      return true;
    else
      return false;
  },

  priorityMedium: function() {
    if (this.priority === 'Medium')
      return true;
    else
      return false;
  },

  priorityLow: function() {
    if (this.priority === 'Low')
      return true;
    else
      return false;
  }
});

/*****************************************************************************/
/* IssueList: Lifecycle Hooks */
/*****************************************************************************/
// Template.IssueList.created = function () {
// };

// Template.IssueList.rendered = function () {
// };

// Template.IssueList.destroyed = function () {
// };
